---
name: Paul Everitt
talks:
- 'Customizing Sphinx: Simple, Normal, and Hard'
---

Paul is the PyCharm/WebStorm Developer Advocate at JetBrains. Before that, Paul was a co-founder of Zope Corporation, taking the first open source application server through $14M of funding. Paul has bootstrapped both the Python Software Foundation and the Plone Foundation. Paul was an officer in the US Navy, starting www.navy.mil in 1993.