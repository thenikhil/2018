---
name: Aileen Nielsen
talks:
- The Tricky Business of Not Discriminating
---

Aileen has worked in corporate law, physics research labs, and, most recently, a variety of NYC tech startups. Her interests range from defensive software engineering to UX designs for reducing cognitive load to the interplay between law and technology. Aileen is currently working at an early-stage NYC startup that has something to do with time series data and neural networks. She also serves as chair of the New York City Bar Association’s Science and Law committee, which focuses on how the latest developments in science and computing should be regulated and how such developments should inform existing legal practices. In the recent past, Aileen worked at mobile health platform One Drop and on Hillary Clinton's presidential campaign. She is a frequent speaker at machine learning conferences on both technical and sociological subjects.