---
abstract: Networks/graphs are one of the most crucial data structures in our increasingly
  intertwined world. Social networks, world-wide web, financial systems, etc. are
  all graph structures. Knowing how to analyze the network topology of interconnected
  systems is an invaluable skill in anyone's toolbox.
duration: 25
level: All
room: Madison
slot: 2018-10-06 15:40:00-04:00
speakers:
- Noemi Derzsy
title: Network/Graph Analysis in Python
type: talk
---

In this tutorial you will learn how to generate, manipulate, analyze and visualize graph structures that will help you gain insight about relationships between elements in your data.

We will start by exploring the basic network types, and the most often encountered network models in real data. We will explore the most informative network measures to understand the network structure and behaviors.

In the second part of the tutorial we will use real public datasets for case-studies. Specifically, we will use public social networks data to construct, analyze and visualize the social network and understand the structure and connectivity behaviors.

This workshop will offer you a hands-on guide on how to approach a network analysis project from scratch and end-to-end:

* generate network structures of various types (directed, weighted, multi graphs, etc.)
* manipulate networks and graph data
* analyze structural network properties
* understand different graph models
* calculate network measures
* apply all the above to real-world case-studies
* visualize networks