---
name: BuzzFeed
tier: gold
site_url: https://tech.buzzfeed.com
logo: buzzfeed.png
---
BuzzFeed is the leading independent digital media company delivering news and entertainment to
hundreds of millions of people around the world. We strive to connect deeply with our audience, and
give them news and entertainment worth sharing with their friends, family, and the people who matter
in their lives.

We have the innovation-obsessed culture and structure of a venture-backed tech company with an
engineering team focused on building the media platform for today’s world, and the future.

Our tech team is ~200 people strong with team members working in locations around the world —
including New York City, Los Angeles, Minneapolis, London, and San Francisco. We’re small enough
that an individual can join our team and have a major impact on our products and culture, and big
enough that there are a ton of brilliant and awesome colleagues to learn from. We rely on
experimentation, robust data collection, and reporting to shape our products, delight our readers
and viewers, inform our content creators, and execute on our business initiatives.
